#include <vector>
#include <set>
#include <algorithm>
#include <utility>

#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include "redblack.h"


TEST(node, left_rotate1) {
    myStruct::rbNode<int> *root = new myStruct::rbNode<int>{2};
    root->left_ = new myStruct::rbNode<int>{1};
    root->right_ = new myStruct::rbNode<int>{3};

    ASSERT_EQ(root->key_, 2);
    ASSERT_EQ(root->left_->key_, 1);
    ASSERT_EQ(root->right_->key_, 3);

    root->left_rotate();
    ASSERT_EQ(root->key_, 3);
    ASSERT_EQ(root->left_->key_, 2);
    ASSERT_EQ(root->left_->left_->key_, 1);

    root->right_rotate();
    ASSERT_EQ(root->key_, 2);
    ASSERT_EQ(root->left_->key_, 1);
    ASSERT_EQ(root->right_->key_, 3);
    delete root->left_;
    delete root->right_;
    delete root;
}


TEST(tree, insert1) {
    myStruct::rbTree<int> tree;
    tree.insert(1);
    tree.insert(2);
    tree.insert(3);
    tree.insert(4);
    for (int i = 1; i <= 4; ++i) {
        ASSERT_EQ(tree.contains(i), true);
    }

}

TEST(tree, insert2) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }

    for (int i = 0; i < test.size(); ++i) {
        ASSERT_EQ(tree.contains(test[i]), true);
    }

    ASSERT_EQ(tree.contains(0), false);
    ASSERT_EQ(tree.size(), test.size());
}

TEST(iterator, begin1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }

    ASSERT_EQ(*tree.begin(), 1);
    tree.insert(-100);
    ASSERT_EQ(*tree.begin(), -100);
    tree.insert(-200);
    ASSERT_EQ(*std::begin(tree), -200);
}

TEST(iterator, increment1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }
    std::sort(test.begin(), test.end());
    int i = 0;
    for (auto it = tree.begin(), ite = tree.end(); it != ite; ++it, ++i) {
        ASSERT_EQ(*it, test[i]);
    }
    i = 0;
    for (auto it = tree.begin(), ite = tree.end(); it != ite; it++, ++i) {
        ASSERT_EQ(*it, test[i]);
    }

}

TEST(iterator, decrement1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }
    std::sort(test.begin(), test.end());
    int i = test.size()-1;
    auto it = tree.end();
    --it;
    for (auto its = tree.begin(); it != its; --it, --i) {
        ASSERT_EQ(test[i], *it);
    }
    ASSERT_EQ(test[0], *tree.begin());
}

TEST(iterator, arrow1) {
    struct comp {
        using S = std::pair<int, int>;
        bool operator() (const S& lhs, const S &rhs) const {
            return lhs.first < rhs.first;
        }
    };

    std::pair<int, int> pair = {1, 2};
    myStruct::rbTree<std::pair<int, int>, comp> tree;
    tree.insert(pair);
    pair.first = 2;
    tree.insert(pair);
    tree.insert(pair);

    ASSERT_EQ(tree.size(), 2);

    auto it = tree.begin();
    ASSERT_EQ(it->first, 1);
    ASSERT_EQ(it->second, 2);
    ++it;
    ASSERT_EQ(it->first, 2);
    ASSERT_EQ(it->second, 2);
    ++it;
    ASSERT_EQ(it, tree.end());
}

TEST(iterator, ranges1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i)
        tree.insert(test[i]);

    auto it = tree.begin();
    for (int el : tree)
        ASSERT_EQ(el, *(it++));

    it = tree.begin();
    std::for_each(tree.begin(), tree.end(), [&](const int &x){ ASSERT_EQ(x, *(it++)); });

}

TEST(iterator, distance1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i)
        tree.insert(test[i]);

    auto it1 = tree.begin(), it2 = tree.end();

    ASSERT_EQ(std::distance(it1, it2), tree.size());
    ASSERT_EQ(std::distance(++it1, --it2), tree.size() - 2);

    it1 = tree.find(1);
    it2 = tree.find(4);
    ASSERT_EQ(std::distance(it1, it2), 1);
}

TEST(tree, ctr0) {
    using rbTree = myStruct::rbTree<int>;

    rbTree tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }

    rbTree tree2;
    tree2 = tree;
}


TEST(tree, ctr1) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }

    tree = tree;
    tree = std::move(tree);

    myStruct::rbTree tree2(tree);
    myStruct::rbTree tree3 = tree;
    myStruct::rbTree<int> tree4 = std::move(tree3);
    myStruct::rbTree<int> tree5;
    tree5 = std::move(tree4);

    auto it = tree.begin();
    for (auto el : tree5) {
        ASSERT_EQ(el, *it);
        ++it;
    }

    tree5.insert(-100);
    tree5.insert(-200);
    tree = tree5;

    it = tree.begin();
    for (auto el : tree5) {
        ASSERT_EQ(el, *it);
        ++it;
    }

}

TEST(tree, ctr2) {
    myStruct::rbTree<int> tree;
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101};
    for (int i = 0; i < test.size(); ++i) {
        tree.insert(test[i]);
    }

    //
    myStruct::rbTree<int> tree1;
    tree1 = std::move(tree);
    myStruct::rbTree<int> tree2 = std::move(tree1);
    //
}

TEST(tree, it_ctr1) {
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101, -23, -6};

    myStruct::rbTree<int> tree(test.begin(), test.end());

    std::sort(test.begin(), test.end());

    ASSERT_EQ(tree.size(), test.size());

    auto test_it = test.begin();
    for (auto it = tree.begin(), end = tree.end(); it != end; ++it, ++test_it)
        ASSERT_EQ(*it, *test_it);
}

TEST(tree, it_ctr2) {
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101, -23, -6};
    std::set<int> test_set(test.begin(), test.end());

    myStruct::rbTree<int> tree(test_set.begin(), test_set.end());

    ASSERT_EQ(tree.size(), test_set.size());

    auto test_it = test_set.begin();
    for (auto it = tree.begin(), end = tree.end(); it != end; ++it, ++test_it)
        ASSERT_EQ(*it, *test_it);
}

TEST(tree, clear1) {
    std::vector<int> test = {391, 1, 4, 203, 5, 250, 101, -23, -6};

    myStruct::rbTree<int> tree(test.begin(), test.end());
   
    ASSERT_EQ(tree.size(), test.size());
    
    tree.clear();
    ASSERT_EQ(tree.size(), 0);
}

int main(int argc, char **argv) {
	::testing::InitGoogleTest(&argc, argv);
	::testing::InitGoogleMock(&argc, argv);

	return RUN_ALL_TESTS();
}
