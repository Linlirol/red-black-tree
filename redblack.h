#pragma once
#include <iostream>
#include <stack>
#include <exception>
#include <algorithm>

namespace myStruct {

enum class Color : unsigned char {
    BLACK,
    RED,
};

template <class KeyT>
struct rbNode final {
    KeyT key_;
    Color color_ = Color::BLACK;
    rbNode *parent_ = nullptr, *left_ = nullptr, *right_ = nullptr;

    void left_rotate();
    void right_rotate();

    rbNode* find_max() const;
    rbNode* find_min() const;
};

template <class KeyT>
void rbNode<KeyT>::left_rotate() {
    if (!right_)
        return;

    std::swap(key_, right_->key_); std::swap(color_, right_->color_);
    rbNode<KeyT> *tmp = left_;

    left_ = right_;
    right_ = right_->right_;
    if (right_)
        right_->parent_ = this;

    std::swap(left_->left_, left_->right_);
    left_->left_ = tmp;
    if (left_->left_)
        left_->left_->parent_ = left_;
}

template <class KeyT>
void rbNode<KeyT>::right_rotate() {
    if (!left_)
        return;
    std::swap(key_, left_->key_); std::swap(color_, left_->color_);
    rbNode<KeyT> *tmp = right_;

    right_ = left_;
    left_ = left_->left_;
    if (left_)
        left_->parent_ = this;

    std::swap(right_->left_, right_->right_);
    right_->right_ = tmp;
    if (right_->right_)
        right_->right_->parent_ = right_;
}

template <class KeyT>
rbNode<KeyT>* rbNode<KeyT>::find_max() const {
    rbNode* cp_node = this;
    if (!cp_node) return this;
    while (cp_node->right_)
        cp_node = cp_node->right_;
    return cp_node;
}

template <class KeyT>
rbNode<KeyT>* rbNode<KeyT>::find_min() const {
    rbNode* cp_node = this;
    if (!cp_node) return this;
    while (cp_node->left_)
        cp_node = cp_node->left_;
    return cp_node;
}

/* Class Red-Black tree */
template <class KeyT, class Comparator = std::less<KeyT>>
class rbTree final {
public: // typedefs
    using key_type = KeyT;
    using value_type = KeyT;
    using size_type = std::size_t;

private:
    using Node = rbNode<KeyT>;
    Comparator key_compare = Comparator{};

    std::size_t size_ = 0;

    Node *root_ = nullptr;

    // TODO: add rb_insert and rb_balance methods ?
    Node* find_parent(const KeyT& key) const;
    void insert_balance(Node* node);
    void erase_balance(Node* node);

    void swap(rbTree &lhs, rbTree &rhs) noexcept;

public: // iterator

    class rbIterator final {
    public: // iterator_traits
        using iterator_category = std::bidirectional_iterator_tag;
        using value_type = KeyT;
        using difference_type = std::ptrdiff_t;
        using pointer = KeyT*;
        using reference = KeyT&;
    private:
        friend class rbTree<KeyT, Comparator>;
        const Node *node_ = nullptr;
        const rbTree<KeyT, Comparator> *tree_ = nullptr;

        rbIterator(Node* node, const rbTree<KeyT, Comparator> *root) : node_{node}, tree_{root} {};

    public:
        [[nodiscard]] bool operator==(const rbIterator& rhs) const noexcept { return node_ == rhs.node_; }
        [[nodiscard]] bool operator!=(const rbIterator& rhs) const noexcept { return node_ != rhs.node_; }

        /* *it return value of iterated element; (!) if end() is dereferenced, *rbegin() value is returned */
        [[nodiscard]] const KeyT& operator*() const;

        /* Pre-increment */
        rbIterator& operator++() &;

        /* Post-increment */
        rbIterator operator++(int);

        /* Pre-decrement */
        rbIterator& operator--() &;

        /* Post-decrement */
        rbIterator operator--(int);

        const KeyT* operator->() { return &(node_->key_); }

    }; // class rbIterator;

private: // iterator
    rbIterator begin_ = {nullptr, this};
    rbIterator rbegin_ = {nullptr, this};

    void update_iterators();
public: // iterator // TODO: reverse iterator
    using const_iterator = rbIterator;
    using iterator = const_iterator ;

    /* Iterator to the first element (inorder) */
    iterator begin() const & { return begin_; }
    /* Iterator past the end of tree */
    iterator end() const & { return iterator(nullptr, this); }

public: // capacity
    [[nodiscard]] bool empty() const noexcept { return size_ == 0; }
    [[nodiscard]] std::size_t size() const noexcept { return size_; }

public: // ctor && dtor
    rbTree() {}

    rbTree(const rbTree& rhs);

    template<class It>
    rbTree(It begin, It end);

    rbTree(rbTree&& rhs) noexcept : root_{rhs.root_}, size_{rhs.size_}, begin_{rhs.begin_},
        rbegin_{rhs.rbegin_} { rhs.root_ = nullptr; }

    rbTree& operator=(const rbTree& rhs);
    rbTree& operator=(rbTree&& rhs) noexcept;

    ~rbTree();
public: // modifier
    void insert(const KeyT& key);
    // TODO: erase implementation
    void erase(const KeyT& key);

    void clear();
public: // lookup
    [[nodiscard]] bool contains(const KeyT& key) const;
    [[nodiscard]] iterator find(const KeyT& key) const &;
}; // class rbTree


/* Iterator */
template<class KeyT, class Comparator>
[[nodiscard]] const KeyT& rbTree<KeyT, Comparator>::rbIterator::operator*() const {
    if (!node_) {
        if (!tree_->root_)
            throw std::length_error("Trying to iterate over empty tree\n");
        return tree_->rbegin_.node_->key_;
    }
    return node_->key_;
}

template<class KeyT, class Comparator>
typename rbTree<KeyT, Comparator>::rbIterator& rbTree<KeyT, Comparator>::rbIterator::operator++() & {
    if (!node_)
        return *this;

    if (node_->right_) {
        node_ = node_->right_;
        while (node_->left_)
            node_ = node_->left_;
        return *this;
    }
    while (node_->parent_ && node_ == node_->parent_->right_)
        node_ = node_->parent_;
    node_ = node_->parent_;
    return *this;
}


template<class KeyT, class Comparator>
typename rbTree<KeyT, Comparator>::rbIterator rbTree<KeyT, Comparator>::rbIterator::operator++(int) {
    rbIterator tmp = *this;
    ++(*this);
    return tmp;
}
template<class KeyT, class Comparator>
typename rbTree<KeyT, Comparator>::rbIterator& rbTree<KeyT, Comparator>::rbIterator::operator--() & {
    if (!node_) {
        *this = tree_->rbegin_;
        return *this;
    }
    if (node_->left_) {
        node_ = node_->left_;
        while (node_->right_)
            node_ = node_->right_;
        return *this;
    }
    while (node_->parent_ && node_ == node_->parent_->left_)
        node_ = node_->parent_;
    node_ = node_->parent_;
    return *this;
}
template<class KeyT, class Comparator>
typename rbTree<KeyT, Comparator>::rbIterator rbTree<KeyT, Comparator>::rbIterator::operator--(int) {
    rbIterator tmp = *this;
    --(*this);
    return tmp;
}



/* rbTree */

template<class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::update_iterators() {
    // TODO: fix O(log n) complexity;
    Node *cp_root = root_;
    while (cp_root->left_)
        cp_root = cp_root->left_;
    begin_.node_ = cp_root;

    cp_root = root_;
    while (cp_root->right_)
        cp_root = cp_root->right_;
    rbegin_.node_ = cp_root;
}

template<class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::swap(rbTree &lhs, rbTree &rhs) noexcept {
    std::swap(lhs.root_, rhs.root_);
    std::swap(lhs.size_, rhs.size_);
    std::swap(lhs.begin_, rhs.begin_);
    std::swap(lhs.rbegin_, rhs.rbegin_);

//    std::cout << "mySwap called\n";
}

template<class KeyT, class Comparator>
rbNode<KeyT>* rbTree<KeyT, Comparator>::find_parent(const KeyT& key) const {
    if (!root_)
        return root_;
    Node *cp_root = root_;
    while (cp_root) {
        if (cp_root->key_ == key)
            return cp_root->parent_;
        if (key_compare(key, cp_root->key_)) {
            if (!cp_root->left_)
                return cp_root;
            cp_root = cp_root->left_;
        }
        else {
            if (!cp_root->right_)
                return cp_root;
            cp_root = cp_root->right_;
        }
    }
    return nullptr;
}

template<class KeyT, class Comparator>
rbTree<KeyT, Comparator>::rbTree(const rbTree& rhs) {
//    std::cout << "rbTree(rbTree& rhs) called\n";
    if (!rhs.root_)
        return;
    root_ = new Node{rhs.root_->key_};

    std::stack<Node*> stack_lhs;
    std::stack<Node*> stack_rhs;

    stack_lhs.push(root_);
    stack_rhs.push(rhs.root_);

    while (!stack_rhs.empty()) {
        Node *cp_lhs = stack_lhs.top();
        Node *cp_rhs = stack_rhs.top();
        stack_lhs.pop();
        stack_rhs.pop();

        if (cp_rhs->left_) {
            cp_lhs->left_ = new Node{cp_rhs->left_->key_, cp_rhs->left_->color_, cp_lhs};

            stack_lhs.push(cp_lhs->left_);
            stack_rhs.push(cp_rhs->left_);
        }

        if (cp_rhs->right_) {
            cp_lhs->right_ = new Node{cp_rhs->right_->key_, cp_rhs->right_->color_, cp_lhs};

            stack_lhs.push(cp_lhs->right_);
            stack_rhs.push(cp_rhs->right_);
        }
    }

    update_iterators();
    this->size_ = rhs.size_;
}

template<class KeyT, class Comparator>
template<class It>
rbTree<KeyT, Comparator>::rbTree(It begin, It end) {
    rbTree tmp;
    std::for_each(begin, end, [&](const KeyT & x){ tmp.insert(x); });
    swap(*this, tmp);
}

template<class KeyT, class Comparator>
rbTree<KeyT, Comparator>& rbTree<KeyT, Comparator>::operator=(const rbTree& rhs) {
//    std::cout << "operator=(rbTree& rhs): " << this << "\n";
    if (this != &rhs) {
        rbTree<KeyT, Comparator> tmp(rhs);
        swap(*this, tmp);
    }

    return *this;
}

template<class KeyT, class Comparator>
rbTree<KeyT, Comparator>& rbTree<KeyT, Comparator>::operator=(rbTree&& rhs) noexcept {
//    std::cout << "operator=(rbTree&& rhs) called\n";
    if (this != &rhs) {
        swap(*this, rhs);
    }

    return *this;
}

template<class KeyT, class Comparator>
rbTree<KeyT, Comparator>::~rbTree() {
//    std::cout << "~rbTree() called: " << this << "\n";
    // in-order traversal
    if (!root_) {
//        std::cout << "--nullptr\n";
        return;
    }

    std::stack<Node*> stack;
    Node *cp_root = root_;
    while (cp_root || !stack.empty()) {
        while (cp_root) {
            stack.push(cp_root);
            cp_root = cp_root->left_;
        }
        cp_root = stack.top();
        stack.pop();
        Node* tmp = cp_root->right_;
        delete cp_root;
        cp_root = tmp;
    }
}

template <class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::insert_balance(Node* cp_root) {
    Node* cp_parent = cp_root->parent_;

    // TODO: benchmark balancing
    while (cp_parent && cp_parent->color_ == Color::RED) {
        Node *cp_gparent = cp_parent->parent_;


        //  Z = red, Z.parent = red, Z.uncle = red -> recolor parent and uncle & grandparent
        if (cp_gparent->left_ && cp_gparent->right_ && (cp_gparent->left_->color_ == cp_gparent->right_->color_)) {
            cp_gparent->color_ = Color::RED;
            cp_gparent->left_->color_ = Color::BLACK;
            cp_gparent->right_->color_ = Color::BLACK;

            cp_root = cp_gparent;
            cp_parent = cp_gparent->parent_;
            continue;
        }

        // Z = red, Z.parent = red, Z.uncle = black (triangle, line)
        if (cp_parent == cp_gparent->left_) {
            // case TRIANGLE
            if (cp_root == cp_parent->right_)
                cp_parent->left_rotate();
            // case LINE
            cp_gparent->right_rotate();
            cp_gparent->color_ = Color::BLACK;
            cp_gparent->right_->color_ = Color::RED;
            break;
        }
        else {
            // case TRIANGLE
            if (cp_root == cp_parent->left_)
                cp_parent->right_rotate();
            // case LINE
            cp_gparent->left_rotate();
            cp_gparent->color_ = Color::BLACK;
            cp_gparent->left_->color_ = Color::RED;
            break;
        }
    }
    root_->color_ = Color::BLACK;
}


template <class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::insert(const KeyT& key) {
    Node *cp_root = nullptr;
    Node *cp_parent = find_parent(key);

    if (cp_parent &&  ((cp_parent->left_ && cp_parent->left_->key_ == key) ||
            (cp_parent->right_ && cp_parent->right_->key_ == key)))
        return;

    cp_root = new Node{key, Color::RED, cp_parent};
    ++size_;

    if (!cp_parent) {
        root_ = cp_root;
        root_->color_ = Color::BLACK;
        update_iterators();
        return;
    }
    else if (key_compare(key, cp_parent->key_))
        cp_parent->left_ = cp_root;
    else
        cp_parent->right_ = cp_root;

    insert_balance(cp_root);

    // iterators;
    update_iterators();
}

#if 0
template<class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::erase_balance(Node* node){ // TODO: 2 more cases + code for "Left child deleted"
    if (!node)
        return;

    std::stack<Node*>  left_balance;
    std::stack<Node*> right_balance;

    /**  Right child deleted:  **/
    if (!node->right_)
        right_balance.push(node);
    else
        left_balance.push(node);

    while (!right_balance.empty() || !left_balance.empty()) {
        if (!right_balance.empty()) {
            node = right_balance.top();
            right_balance.pop();

            Node* child = node->left_;

            /* RED node */
            if (node->color_ == Color::RED) {
                // Child - BLACK with BLACK children
                if (   child->color_ ==  Color::BLACK &&
                     (!child->left_  || (child->left_->color_  == Color::BLACK)) &&
                     (!child->right_ || (child->right_->color_ == Color::BLACK))
                   ) {
                    std::swap(node->color_, child->color_);
                    continue;
                }

                // Child - BLACK with left RED subchild
                if (   child->color_        == Color::BLACK &&
                      (child->left_         &&
                      (child->left_->color_ == Color::RED))
                   ) {
                    child->left_->color_ = Color::BLACK;
                    node->right_rotate();
                    continue;
                }

                // Child - BLACK with right RED subchild -> make it 'B-B-5'
                if (   child->color_        == Color::BLACK &&
                      (child->right_         &&
                      (child->right_->color_ == Color::RED))
                   ) {
                    node->color_ = Color::BLACK;

                    if (node->parent_ && node->parent_->left_ = node)
                        right_balance.push(node->parent_);
                    else
                        left_balance.push(node->parent_);
                    continue;

                }

            } // RED node


            /* BLACK node */

            // Child - RED with right BLACK subchild (child is RED -> subchildren are BLACK && aren't NULL)
            if (child->color_ == Color::RED) {
                // subchild with BLACK sub_sub_children
                if (   (!child->right_->left_  || (child->right_->left_->color_ == Color::BLACK)) &&
                       (!child->right_->right_ || (child->right_->right_->color_ == Color::BLACK))
                   ) {
                    std::swap(child->color_, child->right_->color_);
                    node.right_rotate();
                    continue;
                }

                // subchild with left RED sub_sub_child
                if (   child->right_->left_         &&
                      (child->right_->left_->color_ == Color::RED)
                   ) {
                    child->right_->left_->color_ = Color::BLACK;

                    child.left_rotate();
                    node.right_rotate();
                    continue;
                }
            }

            // Child - BLACK with right RED subchild
            if (   (child->color_ == Color::BLACK) &&
                    child->right_                  &&
                   (child->right_->color_ == Color::RED)
               ) {
                child->right_->color_ = Color::BLACK;

                child.left_rotate();
                node.right_rotate();
                continue;
            }

            // Child - BLACK with BLACK subchildren
            if (     (child->color_ == Color::BLACK)                           &&
                    (!child->left_  || (child->left_->color_ == Color::BLACK)) &&
                    (!child->right_ || (child->right_->color_ == Color::BLACK))
               ) {
                    child->color_ = Color::RED;

                    if (node->parent_ && node->parent_->left_ = node)
                        left_balance.push(node->parent_);
                    else
                        right_balance.push(node->parent_);
                    continue;
            }
        }

        /**  Left child deleted  **/ // to be done
        if (!left_balance.empty()) {
            node = left_balance.top();
            left_balance.pop();
        }
    }
}
#endif

template<class KeyT, class Comparator>
[[nodiscard]] typename rbTree<KeyT, Comparator>::iterator rbTree<KeyT, Comparator>::find(const KeyT& key) const & {
    Node* cp_root = find_parent(key);
    if (!cp_root) {
        if (!root_)
            return end();
        return iterator{root_, this};
    }

    if (key_compare(key, cp_root->key_)) {
        if (!cp_root->left_)
            return end();
        return iterator{cp_root->left_, this};
    }

    if (!cp_root->right_)
        return end();
    return iterator{cp_root->right_, this};

}

template<class KeyT, class Comparator>
[[nodiscard]] bool rbTree<KeyT, Comparator>::contains(const KeyT& key) const {
    return !(find(key) == end());
}



#if 0
template<class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::erase(const KeyT& key) {
    auto cp_it = find(key);
    if (cp_it == end()) return;

    --size_;
    while (true) {
        Node* el = cp_it.node_;

        /**  case RED: 0 or 2 children  **/
        if (el->color_ == Color::RED) { // => el != root_
            /* 0 children: no balancing required */
            if (el->left_ == nullptr && el->right_ == nullptr) {
                Node* cp_parent = el->parent_;
                if (cp_parent->left_ == el)
                    cp_parent->left_ = nullptr;
                else
                    cp_parent->right_ = nullptr;

                delete el;

                update_iterators();
                return;
            }

            /* 2 children: find MIN element of right subtree */
            ++cp_it;
            std::swap(el->key_, cp_it.node_->key_);
            continue;
        }

        /** case BLACK: 0 children OR 1 child OR 2 children **/

        /* 2 children -> similar to case RED */
        if (el->left_ && el->right_) {
            ++cp_it;
            std::swap(el->key_, cp_it.node_->key_);
            continue;
        }

        /* 1 child -> other is null -> child is RED and has no children */
        if (el->left_ && !el->right_) {
            std::swap(el->key_, el->left_->key_);
            cp_it.node_ = el->left_;
            continue;
        }
        if (!el->left_ && el->right_) {
            std::swap(el->key_, el->right_->key_);
            cp_it.node_ = el->right_;
            continue;
        }

        /* 0 children. Balancing required: TODO */
        Node* cp_parent = el->parent_;

        /* element is root => no balancing required */
        if (!cp_parent) {
            delete el;
            root_ = nullptr;
            return;
        }

        /* element is left child */
        if (el == cp_parent->left_)
            cp_parent->left_ = nullptr;
        /* element is right child  */
        else
            cp_parent->right_ = nullptr;

        delete el;

        erase_balance(cp_parent);

        update_iterators();
        return;
    }
}
#endif

template<class KeyT, class Comparator>
void rbTree<KeyT, Comparator>::clear() {
    rbTree tmp;
    swap(*this, tmp);
}

} // namespace myStruct
