add_executable(run_tests test.cpp)
add_test(NAME run_tests COMMAND run_tests)
target_link_libraries(run_tests redblack gtest gmock pthread)
