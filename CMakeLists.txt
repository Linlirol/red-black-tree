cmake_minimum_required(VERSION 3.20)
set(CMAKE_CXX_STANDARD 17)
project(redblacktree)

add_library(redblack INTERFACE)
target_include_directories(redblack INTERFACE ${CMAKE_CURRENT_SOURCE_DIR})

set_target_properties(redblack PROPERTIES PUBLIC_HEADER redblack.h)

if (TESTS)
    enable_testing()
    add_subdirectory(tests)
    message("TESTING ENABLED")
endif()
