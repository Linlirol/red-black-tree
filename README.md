# Red-black tree
Header-only library "red-black-tree" implements stl-compatible class similar to std::set.

## Requirements
* C++17 compiler
* cmake 3.20+
* gtest, gmock

## Building and Testing

Building:
```
git clone https://gitlab.com/Linlirol/red-black-tree.git && cd red-black-tree
cmake -DTESTS=ON -B build && cd build
make
```
Run tests:
```
./tests/run_tests
```

## Using

To use Red-black tree lib in your project you can add these lines to your CMakeLists.txt file:
```
add_subdirectory(YOUR_PATH/red-black-tree)
target_link_libraries(YOUR_LIB redblack)
```
And in your code:
```
#include "redblack.h"
```

## Construction:
```
using namespace myStruct;

rbTree<T> tree; // default ctr

rbTree    tree = other_tree; // ctr from other rbTree
rbTree    tree(other_tree);

rbTree<T> tree(smth.begin(), smth.end()); // ctr from range
```
## Methods implemented:

### Modifiers:
```
/* Insert: inserts elements into the tree */
rbTree<int> tree;
tree.insert(3);
tree.insert(1);
tree.insert(5); // tree = {1, 3, 5}

/* Erase: erases elements from the tree (IN DEV) */
tree.erase(3); // tree = {1, 5}

tree.clear(); // tree = {}
```

### Lookup and capacity:
```
// tree = {1, 5}

/* Size: returns number of elements in a tree */
tree.size(); // 2

/* Empty: checks whether the tree is empty */
tree.empty(); // false

/* Contains: checks whether the element is in the tree */
tree.contains(1); // true
tree.contains(3); // false

/* Find: returns iterator to the element with specified key or end() if it is not in the tree */
auto c_it = tree.find(1); // *c_it = 1
     c_it = tree.find(3); // c_it == tree.end()

```

### Access
You can access the elements of the tree using range based for loops and stl algorithms:
```
// tree = {1, 5}

for (int const el : tree)
    printf("%d ", el); // 1 5

for_each(tree.begin(), tree.end(), [](int const &x){ printf("%d ", el+1); }); // 2 6

for (auto it = tree.begin(), end = tree.end(); it != end; ++it) 
    printf("%d ", *it); // 1 5
```

---
[Google Test](https://github.com/google/googletest)
